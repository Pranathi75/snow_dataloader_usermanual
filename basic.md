
# What is DataLoader?

<!-- > PERFORM DATA MAPPING OF MAP LAYER'S DATA  For more info visit https://snow.fieldon.com/. -->



<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->
<!-- [filename](_media/example.js ':include :type=code :fragment=demo') -->
<!-- ![logo](media/Capture.PNG) -->

DataLoader is used to perform data mapping for the layer's data of a map based on its domain.

1. Map can be created for the below domains:

 - <code>Land</code> 
 - <code>Gas</code> 
 - <code>Water (Drainage)</code> 
 - <code>Water (Water Supply)</code> 

2. Two types of layers can be created for a map.

 - <code>KML layer</code> 
 - <code>Shape layer</code> 

3. Data mapping is done for uploaded data structure and standard data structure which are of same datatype.

4. Migrate the map after data mapping is done. 

Note: DataLoader functionality is available for the web users.



<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->