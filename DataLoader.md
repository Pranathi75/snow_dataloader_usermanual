# DataLoader
<!-- {docsify-ignore} -->
<p>Enter valid Account admin username and password and click on Sign in.</p>

Login as [Account admin](https://snow.fieldon.com.au/#/login "to create map, layers and perform data mapping")

![](media/1.png)

<div align="center"> 

**`Fig.1` Spatial Now Login page**

</div>

- After login we can see the Accounts page, list of below options in the menu bar.

    - <code>Administrators</code>
    - <code>users</code>
    - <code>categories</code>
    - <code>Device management</code>
    - <code>Templates</code>
    - <code>Forms</code>	
    - <code>Projects</code>
    - <code>Tasks</code>
    - <code>Workflow management</code>
    - <code>Projects</code>
    - <code>Downloads</code>
    - <code>DataLoader</code>
    - <code>GSA Lite</code>
    - <code>Settings</code>

<p align="center"> <img width="300" height="380" src="media/2.1.png"> </p>

<div align="center"> 

**`Fig.2` Administartors page**

</div>

<hr>

## DataLoader

1.	Click on the data loader icon on the left-hand menu bar.

2.	 Opens data loader page showing tips on the right side and previous transactions if any with following details: map name, domain, timestamp (UTC), compatibility, status, mapped collections, description. 

<div align="center"> 

**(Or)**

</div>

If there no transactions, shows as No Previous Transactions.

<p align="center"> <img width="300" height="380" src="media/3.1.png"> </p>

<div align="center"> 

**`Fig.3` Tips for DataLoader**

</div>

### Add Map

1.	Click on “Create New map” option, shows Add Map, Add layers, Data Mapping options.

<p align="center"> <img width="300" height="380" src="media/4.1.png"> </p>
   
<div align="center"> 

**`Fig.4` Create New Map**

</div>

2.	Click on Add map, opens create new map page with Map Name, Description, Domain and cancel, save options.


![](media/6.png)

<div align="center"> 

**`Fig.5` Add Map**

</div>

3.	Enter the details map name, domain and click on save button.

<div align="center">

![](media/5.1.png)

</div>

<div align="center"> 

**`Fig.6` create new map page**

</div>

### Add layers

1.	Click on Add layers option, shows Add KML layer, Add Shape layer options.

<div align="center"> 

![](media/7.1.png)

</div>

<div align="center"> 

**`Fig.7` Add layers**

</div>

### Add KML layer

1.	Click on Add KML layer option, opens upload KML layers page.

<div align="center"> 

![](media/8.1.png)

</div>

<div align="center"> 

**`Fig.8` Add KML layer**

</div>

2.	Drag files or choose files to upload. 

3.	Choose files .KML extension, shows upload and cancel buttons.

4.	Click on upload option.


![](media/9.1.png)

<div align="center"> 

**`Fig.9` upload files**

</div>

5.	Files should be added and shows finish and cancel buttons. Click on finish button.

![](media/10.1.png)

<div align="center"> 

**`Fig.10` Added files**

</div>

6.	The application shows source data preview and map information of uploaded files. 

7.	Click on map data to view the information 

![](media/11.png)

<div align="center"> 

**`Fig.11` layer's map data**

</div>

8.	Click on “upload more” option to uploads more layers.

<p align="center"> <img width="340" height="320" src="media/12.1.png"> </p>

<div align="center"> 

**`Fig.12` Upload more layers**

</div>

9.	Application will navigate to Add layers page.

### Add shape layer

1.	Select Add shape layer option, shows already uploaded files if any. 

![](media/13.png)

<div align="center"> 

**`Fig.13` Add shape layer**

</div>

2.	Choose files with extensions as .prj, .shp, .dbf, .shx. Shows upload and cancel buttons

3.	 Click on upload button, files will be added. Shows finish and cancel buttons.

![](media/14.1.png)

<div align="center"> 

**`Fig.14` Upload shape layers**

</div>

4.	Click on finish button.

![](media/15.1.png)

<div align="center"> 

**`Fig.15` Files added**

</div>

5.	Shows data source preview and map view of the files.

![](media/16.1.png)

<div align="center"> 

**`Fig.16` Map view of added layers**

</div>

### Data Mapping

1.	Click on Data Mapping option.

2.	Opens data mapper page showing uploaded data structure and standard data structure with map option.

3.	Select the data structure from the dropdown of uploaded data structure and structure data structure.

![](media/17.1.png)

<div align="center"> 

**`Fig.17` Select the data structure**

</div>

4.	Shows the fields for the selected data structure.

5.	Select the fields from uploaded data structure and structure data structure of same data type and data set and click on map button.

<hr>

![](media/18.1.png)

<div align="center"> 

**`Fig.18` Dataset mapping**

</div>

6.	Data sets will be mapped successfully and shows confirmation message as “1 Data set mapped successfully” and mapped fields are in blue color.

![](media/19.1.png)

<div align="center"> 

**`Fig.19` Data set mapped**

</div>

7.	Click on collapse all button will collapse the mapped data set’s view.

![](media/20.1.png)

<div align="center"> 

**`Fig.20` Collapse mapped datasets**

</div>

8.	Click on expand all button will expand the mapped data set’s view.

<p align="center"> <img width="350" height="290" src="media/21.1.png"> </p>

<div align="center"> 

**`Fig.21` Expand mapped datasets**

</div>

9.	Click on save mapping button will save the mapped data set information.

10.	shows confirmation message as “updated mapping to database”.

![](media/22.1.png)

<div align="center"> 

**`Fig.22` save mapping**

</div>

![](media/23.png)

<div align="center"> 

**`Fig.23` migrate map**

</div>

11.	Click on migrate option, will show confirmation message.

![](media/23.1.png)

<div align="center"> 

**`Fig.24` confirmation  message**

</div>

12.	Click on ok button.

13.	Will navigate to data loaded page showing a message, click on ok.

![](media/24.png)

<div align="center"> 

**`Fig.25` Data mapping process**

</div>

14.	The map information will be visible in the data loader history page with map name, domain, time stamp, compatibility, status, mapped collections, description.

![](media/25.1.png)

<div align="center"> 

**`Fig.26` DataLoader history page**

</div>
